
console.log('Starting to load Example Frame');

var container = document.getElementById('app');
var info = document.getElementById('code');

// Create a listener to receive information from the FactoryFour Application
window.addEventListener('message', function(event) {

	// Check to make sure data is coming from a valid origin
	var validOrigin = [
		'https://app.factoryfour.com',
		'https://lts-app.factoryfour.com',
	].includes(event.origin);

	if (validOrigin && event.data.data) {
		var { data, type } = event.data;

		// Two types of events will be received:
		//  1. an initial message triggered by the fetch data request below
		//  2. an update message triggered by external events pushed to the FactoryFour application like task status changes
		if (type === 'initial' || type === 'update') {
			// Payload of contextual information
			console.log('Received Data', data);

			// Display context in the sample application
			container.classList.add('hide');
			info.classList.remove('hide');
			info.innerText = JSON.stringify(data, null, 4);
		}
	}
}, false);

// Initiate a request for data to the FactoryFour application
// It will respond with data which will be received by the above listener
window.parent.postMessage({
	request: 'FETCH_DATA',
}, '*');

